/**
 * This file is a part of DesQ Clipboard.
 * DesQ Clipboard is the Wlroots-based Clipboard Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include <ClipboardManager.hpp>

namespace WQt {
    class DataControlDevice;
}

namespace DesQ {
    namespace Clipboard {
        class View;
        class Item;
        class Entry;
    }
}

class DesQ::Clipboard::View : public QListView {
    Q_OBJECT;

    public:
        View( DFL::Clipboard::Type );

        /**
         * Delete all existing items. Init the ScrollArea's base with an empty layout
         */
        void reloadView();

    private:
        QStandardItemModel *items;

        DFL::Clipboard::Type mType;
        QSettings *mTable;

    Q_SIGNALS:

        /**
         * Ask the manager to offer data at index @idx from Clipboard type @type
         */
        void offer( DFL::Clipboard::Type type, int idx );
};

class DesQ::Clipboard::Entry {
    public:

        /**
         * Useful for recreating an entry stored in the database The parameter passed is
         * the stringified date-time or QDateTime Format: yyyyMMdd-hhmmss.zzz
         */
        Entry( QString );

        /**
         * Useful for creating a new entry for a given MimeData Date time will be
         * initialized to current date-time
         */
        Entry( QDateTime, WQt::MimeData );

        /**
         * Useful while copying an Entry object. We will inherit the date time of the
         * other Entry.
         */
        Entry( const DesQ::Clipboard::Entry& );

        /**
         * Is this a valid entry?
         * Valid as long as we have a valid date.
         */
        bool isValid();

        /**
         * Get the date-time when this data was created/last offered
         */
        QDateTime dateTime();

        /**
         * Obtain the list of all stored formats. The returned list will be in ascedning
         * order.
         */
        QStringList formats();

        /**
         * Retrieve data corresponding to the given format. If the format does not exist,
         * an empty QByteArray is returned.
         */
        QByteArray data( QString );

        /**
         * Store the format, and the corresponding data Setting an empty data to a format
         * removes it.
         */
        void setData( QString format, QByteArray data );

        /**
         * Clear all the formats stored
         */
        void clear();

        /**
         * Update the date-time to current date time
         */
        void refresh();

        /**
         * Simple way to get the underlying MimeData object
         */
        operator WQt::MimeData();

    private:
        WQt::MimeData mMimeData;
        QDateTime mDateTime;
};
