/**
 * This file is a part of DesQ Clipboard.
 * DesQ Clipboard is the Wlroots-based Clipboard Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ClipboardView.hpp"

#include <desq/Xdg.hpp>
#include <wayqt/DataControl.hpp>

QStringList textFormats =
{ "text/plain", "text/plain;charset=utf-8", "string", "utf8_string" };
QStringList htmlFormats = { "text/html" };

/**
 * Clipboard::View class
 */

DesQ::Clipboard::View::View( DFL::Clipboard::Type type ) : QListView() {
    /** QAbstractScrollArea properties */
    setFrameStyle( QFrame::NoFrame );
    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    /** Palette: Make the base transparent */
    QPalette palette( this->palette() );

    palette.setColor( QPalette::Base, Qt::transparent );
    setPalette( palette );

    /** ListView Properties */
    setFlow( QListView::TopToBottom );
    setMovement( QListView::Static );
    setResizeMode( QListView::Adjust );
    setViewMode( QListView::ListMode );

    items = new QStandardItemModel();
    setModel( items );

    /** Store the type, in case we need it  */
    mType = type;

    /**
     * Create the XDG_DATA_HOME/desq/clipboard path if it doesn't exist
     */
    QString xdgPath = DesQ::XDG::xdgDataHome() + "desq/Clipboard/";

    switch ( type ) {
        case DFL::Clipboard::Clipboard: {
            mTable = new QSettings( xdgPath + "Clipboard.ini", QSettings::IniFormat );
            break;
        }

        case DFL::Clipboard::Selection: {
            mTable = new QSettings( xdgPath + "Selection.ini", QSettings::IniFormat );
            break;
        }
    }

    connect(
        this, &QListView::clicked, [ = ] ( QModelIndex idx ) {
            emit offer( mType, idx.row() );
        }
    );

    /** Reload the view */
    reloadView();
}


void DesQ::Clipboard::View::reloadView() {
    /**
     * We will create a new layout, set it to a widget. This widget will be set as
     * the QScrollArea's widget. The old base is automatically destroyed.
     */

    items->clear();

    mTable->sync();
    /** Get the date-time of the entries */
    QStringList dateTimes = mTable->childGroups();

    /** Sort them in ascending order */
    dateTimes.sort();

    /** Reverse the list: we need the latest date-time first */
    std::reverse( dateTimes.begin(), dateTimes.end() );

    /** Read the entries */
    for ( QString dateTime : dateTimes ) {
        mTable->beginGroup( dateTime );

        /** Accidental empty entry in the ini file */
        if ( not mTable->allKeys().count() ) {
            continue;
        }

        DesQ::Clipboard::Entry mData( dateTime );
        for ( QString format : mTable->allKeys() ) {
            QByteArray hexData( mTable->value( format ).toByteArray() );
            mData.setData( format, QByteArray::fromHex( hexData ) );
        }

        mTable->endGroup();

        QStandardItem *item = new QStandardItem();

        /** Plain text */
        if ( mData.formats().contains( "text/plain" ) ) {
            item->setText( mData.data( "text/plain" ) );
        }

        /** Text list */
        else if ( mData.formats().contains( "text/uri-list" ) ) {
            QStringList uriList =
                QString::fromUtf8( mData.data( "text/uri-list" ) ).trimmed().split( "\n" );
            QString newList = "- " + uriList.join( "\n- " );

            item->setText( newList );
        }

        /** Plain Text */
        else if ( mData.formats().filter( "text/" ).count() ) {
            for ( QString fmt : mData.formats() ) {
                if ( textFormats.contains( fmt ) ) {
                    QString text = QString::fromUtf8( mData.data( fmt ) );
                    item->setText( text );
                    break;
                }
            }
        }

        else if ( mData.formats().filter( "image/" ).count() ) {
            QString    fmt( mData.formats().filter( "image/" ).at( 0 ) );
            QByteArray image( mData.data( fmt ) );
            QPixmap    pix;
            pix.loadFromData( image );

            if ( pix.isNull() ) {
                item->setIcon( QIcon::fromTheme( "image-x-generic" ) );
            }

            else {
                item->setIcon( QIcon( pix ) );
            }
        }

        else {
            QIcon pix;
            for ( QString fmt : mData.formats() ) {
                if ( QIcon::hasThemeIcon( fmt.replace( "/", "-" ) ) ) {
                    pix = QIcon::fromTheme( fmt.replace( "/", "-" ) );
                    break;
                }
            }

            if ( pix.isNull() ) {
                pix = QIcon::fromTheme( "application-octet-stream" );
            }

            item->setIcon( pix );
        }

        items->appendRow( item );
    }

    setCurrentIndex( items->index( 0, 0 ) );
}


DesQ::Clipboard::Entry::Entry( QString strDT ) {
    /** Set the date-time */
    mDateTime = QDateTime::fromString( strDT, "yyyyMMdd-hhmmss.zzz" );
}


DesQ::Clipboard::Entry::Entry( QDateTime dt, WQt::MimeData mimeData ) {
    /** Store the date-time */
    mDateTime = dt;

    /** Store the data */
    mMimeData = mimeData;
}


DesQ::Clipboard::Entry::Entry( const DesQ::Clipboard::Entry& other ) {
    /** Set the date-time */
    mDateTime = other.mDateTime;

    /** Copy the data from @other */
    mMimeData = other.mMimeData;
}


bool DesQ::Clipboard::Entry::isValid() {
    return mDateTime.isValid();
}


QDateTime DesQ::Clipboard::Entry::dateTime() {
    return mDateTime;
}


QStringList DesQ::Clipboard::Entry::formats() {
    return mMimeData.formats();
}


QByteArray DesQ::Clipboard::Entry::data( QString format ) {
    return mMimeData.data( format );
}


void DesQ::Clipboard::Entry::setData( QString format, QByteArray data ) {
    mMimeData.setData( format, data );
}


void DesQ::Clipboard::Entry::clear() {
    mMimeData.clear();
}


void DesQ::Clipboard::Entry::refresh() {
    mDateTime = QDateTime::currentDateTime();
}


DesQ::Clipboard::Entry::operator WQt::MimeData() {
    return mMimeData;
}
