/**
 * This file is a part of DesQ Clipboard.
 * DesQ Clipboard is the Wlroots-based Clipboard Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ClipboardSNI.hpp"

DesQ::Clipboard::SNI::SNI( DesQ::Clipboard::UI *ui ) : QSystemTrayIcon( ui ) {
    setIcon( QIcon( ":/icons/desq-clipboard.png" ) );

    connect(
        this, &QSystemTrayIcon::activated, [ = ] ( QSystemTrayIcon::ActivationReason reason ) {
            switch ( reason ) {
                    case QSystemTrayIcon::Trigger: {
                        if ( ui->isVisible() ) {
                            ui->hide();
                        }

                        else {
                            ui->show();
                        }

                        break;
                    }

                    case QSystemTrayIcon::Context:
                    case QSystemTrayIcon::MiddleClick:
                    case QSystemTrayIcon::DoubleClick:
                    case QSystemTrayIcon::Unknown: {
                        break;
                    }
            }
        }
    );
}
