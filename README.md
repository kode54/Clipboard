# DesQ Clipboard
## A simple clipboard for use DesQ Desktop Environment

This is a simple clipboard manager built using the wlroots wlr-data-control protocol.
Some of it's features are
- Unlimited History
- Prevent Empty Clipboard - Discard the null/invalid entry and continue to offer the older one.
- Track Clipboard and Primary independently - Store their data in independent databases/variables.
- Ignore Primary - Let the compositor handle the Primary according to wayland protocols.
- Disable Primary - Do not allow other apps to access the primary selections of others.
- Pause Clipboard - Do not allow any app to access the clipboard data of other apps.
- Auto-expire entries - Clipboard entries are destroyed after a preset duration.
- Pinned Entries - Entries that are pinned on top of the clipboard. They don't expire.
- Clipboard Actions - Take actions when certain mimetypes are copied to the clipboard.

### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/Clipboard.git DesQClipboard`
- Enter the `DesQClipboard` folder
  * `cd DesQClipboard`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* libdesq (https://gitlab.com/DesQ/libdesq)
* wlrootsqt

### Known Bugs
* Please test and let me know

### Upcoming
* Ignore Primary
* Disable Primary
* Pause Clipboard
* Auto-expire entries
* Pinned Entries
* Clipboard Actions
